var React = require('react');
var ReactDOM = require('react-dom');

var MainAppBlock = React.createClass({
  render: function() {
    return (
		<div className="AppBox">
			Hello, world! I am an AppBox.
		</div>
    );
  }
});

ReactDOM.render(
  <MainAppBlock />,
  document.querySelector( '#mainApp' )
);
/*
require( "./style.css" );

document.write( require( "./src/header.js" ) );
document.write( '<h2>Home Alone!</h2>' );
document.write( require( "./src/content.js" ) );
*/
/* */