# React-Starter #

This README should help with the steps necessary to get your application up and running with React, Babel, and Webpack.

### What is this repository for? ###

* I needed a quick app starting point as I spent the better part of a day getting to the point I could simply right code!
* Version: 2016-09-17-001

### How do I get set up? ###

* Create a new project folder ```mkdir [my-app]```
* Move into this folder ```cd [my-app]```

#### Install NodeJS and NPM ####
Install node.js

node.js comes with a package manager called ```npm```.

#### Install Webpack ####
webpack can be installed through ```npm```:

```
$ npm install webpack -g
```

webpack is now installed globally and the webpack command is available.  You may have to ```sudo```, I did!


#### Use webpack in the Project ####
I have added the package.json here with what you need, but if you do not have one you can ```npm init``` to add one.

Using the package.json I have, you can process to installing it:

```
npm install
```

The app will grab what we need and install it.



### Contact Me ###

* Let me know if you need any help!!