module.exports = {
    entry: "./index.js",
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    module: {
        loaders: [
            {
              loader: "babel-loader",

                exclude: /node_modules/,

              // Only run `.js` and `.jsx` files through Babel
              test: /\.jsx?$/,

              // Options to configure babel with
              query: {
                plugins: ['transform-runtime'],
                presets: ['es2015', 'stage-0', 'react'],
              }
            },
          ]
    }
};